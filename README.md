# Développer une application desktop utilisant 2 API

EXO CDA - NOVEMBRE
Developpement avancé d'une application Desktop

Utilisation de  l'**API** publique de **STAR WARS** 

Partie **publique**

- Une page de **login**
- Une page de **register**
- Une page d'oublie de **mot de passe**
- Une page de ré-initialisation de **mot de passe**

Partie **privée : 9 pages**

- Pages
  - Page **home** → Liste de tous les users de l'applications
  - Page **profile** → Liste toutes vos informations
  - Page **Search** → Page avec un champ de recherche (email) avec les résultats de la recherche
  - Page **Personnages** → Liste tous les personnages de star wars
  - Page **détail** d'un personnage → Liste de détail d'un personnage précis
  - Page **Planètes** → Liste toutes les planètes
  - Page **détail** d'une planète → Liste le détail d'une planète précise
  - Page **Vaisseau →** Liste tous les vaisseaux
  - Page **détail** d'un vaisseau → Liste le détail d'un vaisseau précis
- Un **header** indiquant :
  - Le **nom** de l'utilisateur actuel
  - Indiquant la partie sur laquelle vous vous trouvez
  - Un bouton de **déconnexion**
  - Bonus (Breadcrumb)
- Une **navigation** disponible sur toutes les pages de la partie privée avec :
  - **Home**
  - **Profile**
  - **Search**
  - **Characters** (Personnages)
  - **Planets (\**Planètes\**)**
  - **Ships** (Vaisseaux)

Pour chacune des pages "**liste**", vous devez implémenter :

- Un **filtre** selon les critères de votre choix (3 minimum)
- Une **pagination** (10 éléments par page)
- Une **recherche** selon le nom (name) des éléments de la page en question

# Travail demandé

- Repository (git)
  - Gitlab
  - Github
- Gestion de projet
- Wireframe
- Maquette
- Développement de la partie desktop avec choix de techno
  - React
  - Angular
  - Vue
- Implémentation d'un State manager (Redux)