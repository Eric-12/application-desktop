import React,{useState} from 'react'
import { useLoginMutation } from '../store/services/apiAuth'
import { useLocation, useNavigate } from 'react-router-dom'

// react-bootstrap
import { Form, Button,Container} from 'react-bootstrap';


const LoginPage = () => {

    let navigate = useNavigate();
    let location = useLocation();
    let from = location.state?.from?.pathname || "/personnages";

    const [login, { isLoading, isUpdating }] = useLoginMutation()

    // Info de l'utilisateur
    const [email, setEmail] = useState("lanzae32@gmail.com");
    const [password, setPassword] = useState("123456");

    const body = {
        email,
        password
    }

    console.log(isLoading);
    console.log(isUpdating);

    const handleSubmit = () => {
        console.log('ok')
        login(body).then(() => navigate(from, { replace: true }))
    }


    return (
        <Container>
            <h1>Login</h1>

            <Form> 
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" placeholder="Enter email"  value={email} onChange={setEmail} />
                    <Form.Text className="text-muted">
                        Nous ne partagerons jamais votre e-mail.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={setPassword} />
                </Form.Group>

                <Button variant="primary" type="button" onClick={handleSubmit}>
                    Se connecter
                </Button>
                </Form>
        </Container>
    )
}
export {LoginPage};
