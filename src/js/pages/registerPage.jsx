import React, { useState } from "react";
import { useNavigate } from 'react-router-dom'

import { useRegisterMutation } from '../store/services/apiAuth'


const RegisterPage = () => {

    const navigate = useNavigate()

    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [password, setPassword] = useState("");
    const [username, setUsername] = useState("toto@gmail.com");
    const [email, setEmail] = useState("123456");

    //Api Logic
    const [register, { isLoading, isUpdating }] = useRegisterMutation()

    //Logic
    const [formError, setFormError] = useState(null)

    const handleSubmit = async (e) => {
        e.preventDefault();

        setFormError(null)

        const body = {
            email,
            password,
            username,
            firstname,
            lastname,
        }

        try {
            const result = await register(body)

            if (result.error) {
                return setFormError(result.error.data.message)
            }

            navigate('/login')
        }
        catch (err) {
            console.log('Something went wrong', err);
        }
    }


    return (
        <div>
            <h1>Inscription</h1>

            <Form onSubmit={handleSubmit}> 
                <Form.Group className="mb-3" controlId="formBasicPseudo">
                    <Form.Label>Pseudo</Form.Label>
                    <Form.Control type="text" placeholder="Pseudonyme" onChange={setUsername} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicNom">
                    <Form.Label>Nom</Form.Label>
                    <Form.Control type="text" placeholder="Nom" onChange={setLastname} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPrenom">
                    <Form.Label>Prénom</Form.Label>
                    <Form.Control type="text" placeholder="Prénom" onChange={setFirstname} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={setEmail} />
                    <Form.Text className="text-muted">
                        Nous ne partagerons jamais votre e-mail.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={setPassword} />
                </Form.Group>
                
                <Button variant="primary" type="submit">
                    S'enregistrer
                </Button>
                </Form>
        </div>
    )
}
export {RegisterPage};