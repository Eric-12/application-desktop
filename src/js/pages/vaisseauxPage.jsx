import React from 'react'
import { useFetchVaisseauxQuery } from '../store/services/vaisseaux';
import { Table,Container } from 'react-bootstrap';

const VaisseauxPage = () => {
    const { data =[] , isFetching, } = useFetchVaisseauxQuery()
    console.log(data)

    return(
        <>
            { isFetching && <span>loading</span>}
            <Container>
            <h1 classname="text-danger">Liste des {data.count} vaisseaux de Starwars</h1>
            <hr classname="text-danger" />
            <div>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>nom officiel</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.results &&
                            data.results.map((starships) => (
                            <tr key={starships.name}>
                                <td>{starships.name}</td>
                                <td>{starships.model}</td>
                                <td>{starships.starship_class}</td>
                            </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
            </Container>
        </>
    );
}

export {VaisseauxPage};
