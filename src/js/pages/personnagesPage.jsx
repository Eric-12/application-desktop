import React from 'react'
import { useFetchPersonnagesQuery } from '../store/services/personnages';
import { Table,Container } from 'react-bootstrap';

const PersonnagesPage = () => {
    const { data = [], isFetching, } = useFetchPersonnagesQuery()
    console.log(data)

    return(
        <>
            { isFetching && <span>loading</span>}
            <Container>
                <h1 className="text-warning p-4 text-center">Liste des {data.count} personnages de Starwars</h1>
                <div className="p-2 border border-dark rounded">
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Poids (kg)</th>
                                <th>Taille (cm)</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.results && data.results.map((people) => (
                                    <tr key={people.name}>
                                        <td>{people.name}</td>
                                        <td>{people.mass}</td>
                                        <td>{people.height}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </div>
            </Container>
        </>
    );
}

export {PersonnagesPage};
