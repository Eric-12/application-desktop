import React from 'react'
import { useFetchPlanetesQuery } from '../store/services/planetes';
import { Table,Container } from 'react-bootstrap';

export const PlanetesPage = () => {
    const { data = [], isFetching, } = useFetchPlanetesQuery()
    console.log(data)

    return(
        <>
            { isFetching && <span>loading</span>}
            <Container>
                <h1 className="text-warning p-4 text-center">Liste des {data.count} planètes</h1>
                <div className="p-2 border border-dark rounded">
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Climat</th>
                                <th>Surface de la planète qui est naturellement constitué d'eau (%) </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.results && data.results.map((people) => (
                                    <tr key={people.name}>
                                        <td>{people.name}</td>
                                        <td>{people.climate}</td>
                                        <td>{people.surface_water}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </div>
            </Container>
        </>
    );
}