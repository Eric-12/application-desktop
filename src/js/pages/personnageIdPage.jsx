import React from 'react'
import { useFetchPersonnageQuery} from '../store/services/personnages';
import { Container,ListGroup } from 'react-bootstrap';

const PersonnagePage = () => {
    const { data = [], isFetching, } = useFetchPersonnageQuery()
    console.log('personnage: ' + data)

    return(
        <>
            { isFetching && <span>loading</span>}
            <Container>
                <h1 className="text-warning p-4 text-center">Liste des {data.count} Détail  personnage</h1>
                <div className="p-2 border border-dark rounded">
                    <ListGroup>
                        <ListGroup.Item disabled>{data.name}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.birth_year}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.eye_color}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.gender}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.hair_color}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.height}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.skin_color}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.homeworld}</ListGroup.Item>
                    </ListGroup>
                    {/* ARRAY */}
                    {/* <ListGroup>
                        <ListGroup variant="flush">
                            {data.results && data.results.map((films) => (
                                <ListGroup.Item>Cras justo odio</ListGroup.Item>
                            }
                        </ListGroup>
                        <ListGroup.Item disabled>{data.results.films}</ListGroup.Item>
                    </ ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.results.species}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.results.name}</ListGroup.Item>
                    </ListGroup>

                    <ListGroup>
                        <ListGroup.Item disabled>{data.results.name}</ListGroup.Item>
                    </ListGroup> */}
                    
                </div>
            </Container>
        </>
    );
}
export {PersonnagePage};