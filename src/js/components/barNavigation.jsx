
// components
import React from 'react'; 
import { Navbar,Nav,Container,Offcanvas,FormControl,Form,Button } from 'react-bootstrap';

import { useSelector } from 'react-redux'

const Navigation = () => {

  const auth = useSelector(state => state.auth)


  return(
    <Navbar bg="light" expand={false}>
      <Container fluid>
        <Navbar.Brand href="#">StarWars</Navbar.Brand>
        <Navbar.Toggle aria-controls="offcanvasNavbar" />
        <Navbar.Offcanvas
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
          placement="end"
        >
          <Offcanvas.Header closeButton>
            <Offcanvas.Title id="offcanvasNavbarLabel">StarWars</Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
            <Nav className="justify-content-end flex-grow-1 pe-3">
              <Nav.Link href="/accueil">Accueil</Nav.Link>
                <Nav.Link href="/">Utlisateur</Nav.Link>
                <Nav.Link href="/personnages">Personnages</Nav.Link>
                <Nav.Link href="/personnage">Personnage</Nav.Link>
                <Nav.Link href="/planetes">Planètes</Nav.Link>
                <Nav.Link href="/vaisseaux">Vaisseaux</Nav.Link>
                {/* <Nav.Link href="/login">Se connecter</Nav.Link>
                <Nav.Link href="/register">S'incrire</Nav.Link> */}
              </Nav>
            <Form className="d-flex">
              <FormControl
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
              />
              <Button variant="outline-success">Rechercher</Button>
            </Form>
          </Offcanvas.Body>
        </Navbar.Offcanvas>
      </Container>
    </Navbar>
    
  );
}
export {Navigation};



