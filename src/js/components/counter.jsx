import React from 'react'

const Counter = (props) => {
    const count = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()
    
    return(
        <p>
            <button
                aria-label="Increment value"
                onClick={() => dispatch(increment())}
            >
                Increment
            </button>

            <span>{count}</span>

            <button
                aria-label="Decrement value"
                onClick={() => dispatch(decrement())}
            >
                Decrement
            </button>
        </p>
    );
}

export {Counter}