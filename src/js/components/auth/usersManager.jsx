import React from 'react'
import { useFetchCurrentUserQuery } from '../../store/services/apiAuth'
// react-bootstrap
import { Card, ListGroup,Row } from 'react-bootstrap';

export const UsersManager = () => {

    const { data, isFetching } = useFetchCurrentUserQuery()

    if (isFetching) return <p>Loading</p>

    return (
        <Row>
            <h1 className="text-center p-4 border border-light">Mes informations</h1>
            
            <div className="d-flex justify-content-center">
                <Card style={{ width: '18rem' }} className="bg-warning text-dark">
                    <Card.Body>
                        <Card.Title>Bonjour, {data?.username}</Card.Title>
                        <Card.Subtitle className="mb-2"></Card.Subtitle>
                        <Card.Text>
                        <span className="text-secondary">Nom: </span>{data?.lastname}
                        <br/>
                        <span className="text-secondary">Prénom: </span>{data?.firstname}
                        <br/>
                        <span span className="text-secondary">Email: </span>{data?.email}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        </Row>
    );
}