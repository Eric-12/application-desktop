import React from 'react'

// Gestion du routage (navigation)
import { Routes,Route } from "react-router-dom"

import { useIsAuth } from './hooks/userIsAuth'
import { RequireAuth } from './components/auth/requireAuth'
import { UsersManager } from './components/auth/usersManager'

import {Layout} from './layout/layout'

import {LoginPage} from './pages/loginPage'
import {RegisterPage} from './pages/registerPage'
import {PersonnagesPage} from './pages/personnagesPage'
import {PersonnagePage} from './pages/personnageIdPage'
import {VaisseauxPage} from './pages/vaisseauxPage'
import {PlanetesPage} from './pages/planetesPage'

// styles
import '../css/App.css'

function App() {

    const { isFetching } = useIsAuth();

    console.log('IS FETCHING USER', isFetching);
  
    if (isFetching) return <p>App is Loading</p>

    return (
            <Routes>
                <Route  path="/" element={<Layout />}>  
                    {/* PAGE PUBLIC */}
                    <Route path="/login" element={<LoginPage />} />
                    <Route path="/register" element={<RegisterPage />} />
                    <Route path="/" 
                            element={
                                <RequireAuth>
                                    <UsersManager />
                                </RequireAuth>
                            }
                        />

                    {/* <Route path="/personnages" element={ <PersonnagesPage />} />
                    <Route path="/personnage" element={ <PersonnagePage />} />
                    <Route path="/vaisseaux" element={ <VaisseauxPage />} />
                    <Route path="/planetes" element={ <PlanetesPage />} /> */}

                    {/* PAGE PRIVEE */}
                    {/* <Route path="/" element={<Layout />} > */}
                        <Route path="/" 
                            element={
                                <RequireAuth>
                                    <UsersManager />
                                </RequireAuth>
                            }
                        />
                        <Route path="/personnages" 
                            element={
                                <RequireAuth>
                                    <PersonnagesPage />
                                </RequireAuth>
                            }
                        />
                        <Route path="/personnage" 
                            element={
                                <RequireAuth>
                                    <PersonnagePage />
                                </RequireAuth>
                            }
                        />
                        <Route path="/vaisseaux" 
                            element={
                                <RequireAuth>
                                    <VaisseauxPage />
                                </RequireAuth>
                            }
                        />
                        {/* <Route path="/vaisseau" 
                            element={
                                <RequireAuth>
                                    <VaisseauPage />
                                </RequireAuth>
                            }
                        /> */}
                        <Route path="/planetes" 
                            element={
                                <RequireAuth>
                                    <PlanetesPage />
                                </RequireAuth>
                            }
                        />
                        {/* <Route path="/planete" 
                            element={
                                <RequireAuth>
                                    <VaisseauxPage />
                                </RequireAuth>
                            }
                        /> */}
                    </Route> 
                {/* </Route>  */}
            </Routes> 
    )
}

export default App

