import AsyncStorage from '@react-native-async-storage/async-storage';

// Storing object value
export const setLocalStorage= async (value) => {  
    try {    
        const jsonValue = JSON.stringify(value)    
        await AsyncStorage.setItem('user', jsonValue)  
    } catch (e) {    
        // saving error  
    }
}

// Reading object value
export const getLocalStorage = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('user')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      // error reading value
      console.log(e.getData.value)
    }
}

export const getLocalStorageItem = (key) => {
    return JSON.parse(window.localStorage.getItem(JSON.stringify(key)));
}

export const setLocalStorageItem = async (value, item) => {
    return await window.localStorage.setItem(JSON.stringify(item), JSON.stringify(value));
}

/*
NE FONCTION QUE SUR LES NAVIGATEURS PAS POUR UNE APP MOBILE

export const setLocalStorageItem = async (value, item) => {
    return await window.localStorage.setItem(JSON.stringify(item), JSON.stringify(value));
}

export const getLocalStorageItem = (key) => {
    return window.localStorage.getItem(JSON.stringify(key));
}

export const removeLocalStorageItem = (key) => {
    return window.localStorage.removeItem(JSON.stringify(key));
}
*/  


