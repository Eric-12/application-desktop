import React from 'react'

// Bibliotheque d(icone)
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRightToBracket } from '@fortawesome/free-solid-svg-icons'

// COMPONENTS ROUTER
import { Outlet,Link} from "react-router-dom";

// NAVBAR
import { Navigation } from './../components/barNavigation'

// REACT BOOTSTRAP
import { Container,Row,Col } from 'react-bootstrap';

// Authentification
import { RequireAuth } from './../components/auth/requireAuth'
import { UsersManager } from './../components/auth/usersManager';

//Logo 
import logo from '../../assets/logo.svg'

export const Layout = () => {
    return (
        <>
            <div className="d-flex flex-column bg-dark App-header">
                <Navigation />
                <Container fluid className="">
                    <Row>
                        <Col className="d-flex justify-content-around align-items-center"> 
                            <img src={logo} className="App-logo" alt="logo" />
                            {/* <Link to="login">
                                <FontAwesomeIcon icon={faRightToBracket} size="4x"/>
                            </Link> */}
                        </Col>
                    </Row>
                    
                    <Row>
                        <Col className="flex-grow-1"><Outlet /></Col>
                    </Row>

                    <Row>

                    </Row>
                </Container>
            </div> 
        </>
    );
}
// 