import { configureStore } from '@reduxjs/toolkit'
import logger from 'redux-logger'

import counterReducer from './services/counter'
import { apiPersonnages } from './services/personnages'
import { apiVaisseaux } from './services/vaisseaux'
import {apiPlanete} from './services/planetes'
import {apiAuth} from './services/apiAuth'
import auth from '../components/auth/authSlice'

export const store = configureStore({
    reducer: {
        counter: counterReducer,
        [apiPersonnages.reducerPath]: apiPersonnages.reducer,
        [apiVaisseaux.reducerPath]: apiVaisseaux.reducer,
        [apiPlanete.reducerPath]: apiPlanete.reducer,
        [apiAuth.reducerPath]: apiAuth.reducer,
        auth,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(
        apiPersonnages.middleware,
        apiVaisseaux.middleware, 
        apiPlanete.middleware,
        apiAuth.middleware,
        logger
    )
})