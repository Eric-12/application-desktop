import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiPersonnages = createApi({
    reducerPath: 'api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://swapi.dev/api/'
    }),
    endpoints(builder) {
        return {
            fetchPersonnages: builder.query({
                query(limit = 10) {
                    return `/people?limit=${limit}`;
                }
            }),
            fetchPersonnage: builder.query({
                query(id = 1) {
                    return `/people/${id}`;
                }
            })
        }
    }
})

export const { useFetchPersonnagesQuery, useFetchPersonnageQuery } = apiPersonnages