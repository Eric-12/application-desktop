import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiVaisseaux = createApi({
    reducerPath: 'starshipsApi',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://swapi.dev/api/'
    }),
    endpoints(builder) {
        return {
            fetchVaisseaux: builder.query({
                query(limit = 10) {
                    return `/starships?limit=${limit}`;
                }
            })
        }
    }
})

export const { useFetchVaisseauxQuery } = apiVaisseaux