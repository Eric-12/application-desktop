import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiPlanete = createApi({
    reducerPath: 'api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://swapi.dev/api/'
    }),
    endpoints(builder) {
        return {
            fetchPlanetes: builder.query({
                query(limit = 10) {
                    return `/planets?limit=${limit}`;
                }
            }),
            fetchPlanete: builder.query({
                query(id = 1) {
                    return `/planets/${id}`;
                }
            })
        }
    }
})

export const { useFetchPlanetesQuery, useFetchPlaneteQuery } = apiPlanete