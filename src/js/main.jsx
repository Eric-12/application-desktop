import React from 'react'
import ReactDOM from 'react-dom'
import { store } from './store/store'
import { Provider } from 'react-redux'
// Gestion du routage (navigation)
import { BrowserRouter,Routes,Route } from "react-router-dom"
//components
import App from './App'

// CSS BOOTSTRAP
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter> 
        <App />
      </BrowserRouter>
    </Provider>,
  document.getElementById('root')
)
